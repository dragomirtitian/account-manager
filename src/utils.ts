import { Action, Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { debugDelay } from "./config.json";

export function simpleActionCreator<K extends string>(type: K) {
    return () => action({ type });
}

export function action<T extends {type: K}, K extends string>(a: T) {
    return a;
}

export function createThunkFactory<T>() {
    return function <A extends any[], R>(fn: (...a: A) => (dispatch: ThunkDispatch<T, void, any>, getState: () => T) => R) {
        return fn;
    };
}

export function delay(time: number= debugDelay) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

export function withDispatchFactory<TState, TAction extends Action<any>>() {
    return function<A extends any[], R>(dispatch: ThunkDispatch<TState, void, TAction>, fn: (...a: A) => (dispatch: ThunkDispatch<TState, void, TAction>, getState: () => TState) => R) {
        return function(...a: A): R {
            return dispatch(fn(...a));
        };
    };
}
