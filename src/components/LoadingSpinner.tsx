import React from "react";
import "./LoadingSpinner.css";

function LoadingView(props: { loadingMessage: string | null }) {
    if (!props.loadingMessage) {
        return <></>;
    } else {
        return <div>
            <div className="lds-hourglass"></div>
        </div>;
    }
}

export default LoadingView;
