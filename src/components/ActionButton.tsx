import "./ActionButton.css";

import React from "react";
import { RouteComponentProps, withRouter } from "react-router";

export function ActionButton(p: { icon: string; text: string} & JSX.IntrinsicElements["button"]) {
    const { icon, text, ...buttonProps } = p;
    return <button className="action-button" {...buttonProps}>
        <i className="material-icons">{icon}</i><span>{text}</span>
    </button>;
}

export const ActionButtonLink = withRouter((p: { to: string } & RouteComponentProps & Parameters<typeof ActionButton>[0]) => {
    return <ActionButton {...p} onClick={() => p.history.push(p.to)}></ActionButton>;
});
