import React, { Component } from "react";
import { AccountInfo } from "../reducers/state";
import { ActionButtonLink } from "./ActionButton";
import CreditDebitListView from "./CreditDebitListView";

import "./CreditDebitListViewWithAdd.css";

function CreditDebitListViewWithAdd(p: { accountInfo: AccountInfo | null }) {
    return <div className="region credit-debit-list-view-with-add">
        <div className="region-header">Credits/Debits</div>
        <div className="toolbar">
            <ActionButtonLink to="add-to" icon="add" text="Add Debit" className="action-button xl pill" ></ActionButtonLink>
            <ActionButtonLink to="add-from" icon="add" text="Add Credit" className="action-button xl pill" ></ActionButtonLink>
        </div>
        <div>
            {!p.accountInfo ? <></> :
                <CreditDebitListView items={p.accountInfo.debitsAndCredits}></CreditDebitListView>}
        </div>
    </div>;
}

export default CreditDebitListViewWithAdd;
