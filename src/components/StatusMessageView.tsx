import React from "react";

import "./StatusMessageView.css";

function StatusMessageView(p: { message: string | null, onClick?: () => void }) {
    return !p.message ? <></> :
        <div className="failure-message" onClick={p.onClick}>{p.message.toString()}</div>;
}

export default StatusMessageView;
