import React from "react";
import { CreditDebitItem } from "../reducers/state";

import moment from "moment";

function formatNumber(n: number, d: number) {
  try {
    return Math.abs(n).toFixed(d);
  } catch (r) {
    return n;
  }
}
function CreditDebitView(p: CreditDebitItem) {
    return <div className="credit-debit-view">
      <div className="credit-debit-cell date">{moment(p.date).format("L")}</div>
      <div className="credit-debit-cell time">{moment(p.date).format("HH:MM")}</div>
      {p.from ? <>
        <div className="credit-debit-cell from">{p.from}</div>
        <div className="credit-debit-cell"></div>
        <div className="credit-debit-cell amount-from">{formatNumber(p.amount, 4)}</div>
      </> : <>
        <div className="credit-debit-cell to">{p.to}</div>
        <div className="credit-debit-cell amount-to">{formatNumber(p.amount, 4)}</div>
        <div className="credit-debit-cell"></div>
      </>}
      <div className="credit-debit-cell description">{p.description}</div>
    </div>;
}

export default CreditDebitView;
