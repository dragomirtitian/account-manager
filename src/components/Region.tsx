import React from "react";

import "./Region.css";

function Region(p: { name: string, className?: string; children: React.ReactNode }) {
    return <div className={`region ${p.className || ""}`}>
        <div className="region-header">{p.name}</div>
        <div className="region-content">{p.children}</div>
    </div>;
}

export default Region;
