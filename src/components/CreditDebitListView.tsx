import React from "react";
import { CreditDebitItem } from "../reducers/state";

import CreditDebitView from "./CreditDebitView";

import "./CreditDebitListView.css";

function CreditDebitListView(p: { items: CreditDebitItem[] }) {
    return <div className="credit-debit-view-container">
        <div className="credit-debit-view-table">
            <div className="credit-debit-header">
                <div>Date</div>
                <div>Time</div>
                <div>From/To</div>
                <div className="amount-to">Debit</div>
                <div className="amount-from">Credit</div>
                <div>Description</div>
            </div>
            <div>
                {p.items.map((o, i) => <CreditDebitView {...o} key={i.toString()} />)}
            </div>
        </div>
    </div>;
}

export default CreditDebitListView;
