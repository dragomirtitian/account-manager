import React from "react";
import { AccountInfo } from "../reducers/state";

import "./AccountInfoView.css";

function AccountInfoView(p: { accountInfo: AccountInfo | null }) {
    if (!p.accountInfo) { return <></>; }
    const { name, iban, balance } = p.accountInfo.account;
    return <div className="account-info-display-conatiner">
        <div>
            <span className="label">Name: </span> {name}
        </div>
        <div>
            <span>IBAN: </span> {iban}
        </div>
        <div>
            <span>Balance: </span> {balance}
        </div>
        <div>
            <span>Currency: </span> {p.accountInfo.currency}
        </div>
    </div>;
}

export default AccountInfoView;
