import React, { Component } from "react";

import { connect } from "react-redux";
import { BrowserRouter, Link, Route, RouteComponentProps, RouteProps } from "react-router-dom";
import { ThunkDispatch } from "redux-thunk";
import { clearFailure, withDispatch } from "../actions";
import { fetchAccountInfo } from "../actions/account-info";
import { AccountInfo, AppState } from "../reducers/state";

import AccountInfoView from "../components/AccountInfoView";
import CreditDebitListView from "../components/CreditDebitListView";
import CreditDebitListViewWithAdd from "../components/CreditDebitListViewWithAdd";
import LoadingSpinner from "../components/LoadingSpinner";
import Region from "../components/Region";
import StatusMessageView from "../components/StatusMessageView";
import AddCreditDebitView from "./AddCreditDebitView";

import "./App.css";

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, any>) => {
    return {
        fetchAccountInfo: withDispatch(dispatch, fetchAccountInfo),
        clearFailure: () => { dispatch(clearFailure()); },
    };
};

const mapStateToProps = ({ loadingMessages, failureMessage, accountInfoState: { accountInfo } }: AppState) => ({
    loadingMessage: loadingMessages[0],
    failureMessage,
    accountInfo,
});

type AppProps = ReturnType<typeof mapDispatchToProps>
    & ReturnType<typeof mapStateToProps>;

function Header(p: { children: JSX.Element}) {
    return <div className="app-header header-footer-background">
        <div>
            <h1>ACCOUNT MANAGER</h1>
            <div className="header-content">
                {p.children}
            </div>
        </div>
    </div>;
}

function Footer(p: { children?: React.ReactNode }) {
    return <div className="app-footer header-footer-background">
        {p.children}
    </div>;
}
class App extends Component<AppProps> {
    public componentDidMount() {
        this.props.fetchAccountInfo();
    }
    public render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Header>
                        <LoadingSpinner loadingMessage={this.props.loadingMessage} />
                    </Header>
                    <Region name="Account Info" className="account-info">
                        <AccountInfoView accountInfo={this.props.accountInfo} />
                    </Region>
                    <div className="app-content">
                        <Route path="/add-to" exact render={() => <Region name="New Debit">
                            <AddCreditDebitView type="to"></AddCreditDebitView>
                        </Region>} />
                        <Route path="/add-from" exact render={() => <Region name="New Credit">
                            <AddCreditDebitView type="from"></AddCreditDebitView>
                        </Region>} />
                        <Route path={["/", "/list"]} exact
                            render={() => <CreditDebitListViewWithAdd accountInfo={this.props.accountInfo}></CreditDebitListViewWithAdd>}
                            />
                    </div>
                    <Footer>
                        <StatusMessageView message={this.props.failureMessage} onClick={this.props.clearFailure} />
                        <StatusMessageView message={this.props.loadingMessage} />
                    </Footer>
                </div>
            </BrowserRouter>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
