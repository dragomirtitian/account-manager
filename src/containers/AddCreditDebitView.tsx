import React, { ChangeEvent, Component } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { ThunkDispatch } from "redux-thunk";
import "../date-utils";

import { addCreditDebit, AllActions, withDispatch } from "../actions";
import { AppState, CreditDebitItem } from "../reducers/state";

import { ActionButton, ActionButtonLink } from "../components/ActionButton";

import "./AddCreditDebitView.css";

const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AllActions>) => {
    return {
        addCreditDebit: withDispatch(dispatch, addCreditDebit),
    };
  };

const mapStateToProps = ({ loadingMessages }: AppState) => ({
    isProcessing: loadingMessages.length !== 0,
});

type AddCreditDebitViewProps = ReturnType<typeof mapDispatchToProps>
  & ReturnType<typeof mapStateToProps>
  & RouteComponentProps<any>
  & {
      type: "to" | "from",
  };

type AddCreditDebitViewState =  CreditDebitItem;

function Input<K extends string>(p: {
    containerClassName?: string;
    label: string;
    type?: string;
    name: K;
    value: Record<K, string | number>;
    validation: Record<K, string | true>;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void }) {

    const value = p.value[p.name];
    const hasValue = !(value === "" || value === null || value === undefined);
    return <div className={`input ${p.containerClassName || p.name}`}>
        <input name={p.name} type={p.type || "text"} onChange={p.onChange} value={value} placeholder={p.label}/>
        <label className={!hasValue ? "hidden" : ""}>{p.label}</label>
        {p.validation[p.name] === true ? <></> : <div className="error">{p.validation[p.name]}</div>}
    </div>;
}
class AddCreditDebitView extends Component<AddCreditDebitViewProps, Record<keyof CreditDebitItem, string>> {
    constructor(props: AddCreditDebitViewProps) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onAdd = this.onAdd.bind(this);
        const now = new Date();

        this.state = {
            description: "",
            from: "",
            to: "",
            amount: "",
            date: now.toDatetimeLocal(),
        };
    }
    public async onAdd() {
        await this.props.addCreditDebit(Object.assign({
            description: this.state.description,
            date: this.state.date,
        }, this.props.type === "from" ? {
            amount: parseFloat(this.state.amount),
            from: this.state.from,
        } : {
            amount: -parseFloat(this.state.amount),
            to: this.state.to,
        }));
        this.props.history.push("/");
    }
    public onChange(event: ChangeEvent<HTMLInputElement>) {
        const key = event.target.name as "from";
        this.setState({
            [key] : event.target.value as any,
        });
    }
    public isValid(): Record<keyof AddCreditDebitViewState, string | true> {
        const state = this.state;
        function multi(conditions: Array<string | true>) {
            return conditions.find(o => o !== true) || true;
        }
        return {
            from: this.props.type !== "from" || !!state.from || "From is required.",
            to: this.props.type !== "to" || !!state.to || "To is required.",
            amount: multi([
                !!state.amount || "Amount is required.",
                !isNaN(state.amount as any) || "Amount should be a number.",
                +state.amount > 0 || "Amount should be a pozitive.",
            ]),
            date: multi([
                !!state.date || "Date is required.",
                !isNaN(Date.parse(state.date)) || "Date should be a valid date.",
            ]),
            description: !!state.description || "Description is required.",
        };
    }

    public render() {
        const validation = this.isValid();
        const allValid = Object.values(validation).every((o) => o === true);
        return <div className="add-credit-debit-form">
            <Input label="Description " name="description" value={this.state} onChange={this.onChange} validation={validation}></Input>
            {this.props.type === "from" ?
                <Input label="From" name="from" value={this.state} onChange={this.onChange} validation={validation}></Input> :
                <Input label="To " name="to" value={this.state} onChange={this.onChange} validation={validation}></Input>}
            <Input label="Amount " name="amount" value={this.state} onChange={this.onChange} validation={validation}></Input>
            <Input label="Date " name="date" type="datetime-local" value={this.state} onChange={this.onChange} validation={validation}></Input>
            <div className="toolbar">
                <ActionButton onClick={this.onAdd} icon="save" text="Save" disabled={!allValid || this.props.isProcessing}></ActionButton>
                <ActionButtonLink to="/" icon="cancel" text="Cancel" disabled={this.props.isProcessing}></ActionButtonLink>
            </div>
        </div>;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddCreditDebitView));
