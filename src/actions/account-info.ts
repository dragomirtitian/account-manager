import { clearFailure } from ".";
import { AccountInfo, AppState } from "../reducers/state";
import { action, createThunkFactory, delay, simpleActionCreator } from "../utils";
import { serverUrl } from "./../config.json";
import { raiseFailure } from "./failure";
import { beginLoading, endLoading } from "./loading";

export const requestAccountInfo = simpleActionCreator("REQUEST_ACCOUNT_INFO");
export const failedAccountInfo = simpleActionCreator("FAILED_ACCOUNT_INFO");

export const receiveAccountInfo = (payload: AccountInfo) => action({
    type: "RECEIVE_ACCOUNT_INFO",
    payload,
});

const createThunk = createThunkFactory<AppState>();

export const fetchAccountInfo = createThunk((retry: number = 3) => async (dispatch) => {
    dispatch(beginLoading("Loading data"));
    dispatch(requestAccountInfo());
    try {
        await delay();
        const respone = await fetch(`${serverUrl}/api/balance`, {
            cache: "no-cache",
        });
        const data: AccountInfo = await respone.json();
        dispatch(receiveAccountInfo(data));
        dispatch(endLoading());
        dispatch(clearFailure());
    } catch (e) {
        dispatch(raiseFailure(e || e.message));
        dispatch(endLoading());
        if (retry !== 0) {
            dispatch(fetchAccountInfo(retry - 1));
        }
    }
});
