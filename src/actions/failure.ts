import { action, simpleActionCreator } from "../utils";

export const raiseFailure = (message: string) => action({
    type: "RAISE_FAILURE",
    message,
});

export const clearFailure = simpleActionCreator("CLEAR_FAILURE");
