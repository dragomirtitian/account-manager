import { action, simpleActionCreator } from "../utils";

export const beginLoading = (message: string) => action({
    type: "LOADING_START",
    message,
});
export const endLoading = simpleActionCreator("LOADING_END");
