import { AppState, CreditDebitItem } from "../reducers/state";
import { createThunkFactory, delay } from "../utils";
import { serverUrl } from "./../config.json";
import { fetchAccountInfo } from "./account-info";
import { clearFailure, raiseFailure } from "./failure";
import { beginLoading, endLoading } from "./loading";

const createThunk = createThunkFactory<AppState>();

export const addCreditDebit = createThunk((data: CreditDebitItem, retry: number = 3) => async (dispatch) => {
    dispatch(beginLoading("Adding data"));
    try {
        await delay();
        await fetch(`${serverUrl}/api/balance/add`, {
            method: "PUT",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
        dispatch(endLoading());
        dispatch(clearFailure());
        dispatch(fetchAccountInfo());
    } catch (e) {
        dispatch(endLoading());
        dispatch(raiseFailure(e.message || e));
        if (retry > 0) {
            dispatch(addCreditDebit(data, retry - 1));
        }
    }
});
