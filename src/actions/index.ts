// tslint:disable:ordered-imports
import { Action } from "redux";
import { withDispatchFactory } from "../utils";
import { AppState } from "../reducers/state";

import * as account from "./account-info";
import * as loading from "./loading";
import * as failure from "./failure";

import * as creditDebit from "./credit-debit";

export * from "./account-info";
export * from "./credit-debit";
export * from "./loading";
export * from "./failure";

type ReturnTypeOrNever<T> = T extends (...a: any[]) => infer R ? R : never;
export type ModuleActions<M> = M extends any ? Extract<ReturnTypeOrNever<M[keyof M]>, Action> : never;

export type AccountActions = ModuleActions<typeof account>;
export type LoadingActions = ModuleActions<typeof loading>;
export type CreditDebitActions = ModuleActions<typeof creditDebit>;
export type FailureActions = ModuleActions<typeof failure>;
export type AllActions = ModuleActions<typeof account | typeof loading | typeof creditDebit | typeof failure>;

export const withDispatch = withDispatchFactory<AppState, AllActions>();
