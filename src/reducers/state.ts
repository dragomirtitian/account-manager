import { string } from "prop-types";

export type CreditDebitItem = {
    to?: undefined;
    from: string;
    description: string;
    amount: number;
    date: string;
} | {
    from?: undefined;
    to: string;
    description: string;
    amount: number;
    date: string;
};

export interface AccountInfo {
    debitsAndCredits: CreditDebitItem[];
    currency: "EURO" | "USD";
    account: {
        name: string;
        iban: string;
        balance: number
    };
}
export interface AccountInfoState {
    accountInfo: AccountInfo | null;
}

export interface AppState {
    accountInfoState: AccountInfoState;
    loadingMessages: string[];
    failureMessage: string | null;
}
