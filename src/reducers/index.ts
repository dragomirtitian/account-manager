import { combineReducers } from "redux";
import { AccountActions, FailureActions, LoadingActions } from "../actions";
import { AccountInfoState, AppState } from "./state";

const defaultAccountInfo = (): AccountInfoState => ({
    accountInfo: null,
});

function updateState<T>(state: T, updates: Partial<T>) {
    return Object.assign({}, state, updates);
}

function accountInfoState(state = defaultAccountInfo(), action: AccountActions): AccountInfoState {
    switch (action.type) {
        case "REQUEST_ACCOUNT_INFO":
            return updateState(state, {
            });
        case "RECEIVE_ACCOUNT_INFO":
            return updateState(state, {
                accountInfo: action.payload,
            });
        case "FAILED_ACCOUNT_INFO":
            return updateState(state, {
            });
    }
    return state;
}

function loadingMessages(state: string[] = [], action: LoadingActions): string[] {
    switch (action.type) {
        case "LOADING_START": return [action.message, ...state];
        case "LOADING_END": return state.slice(1);
    }
    return state;
}

function failureMessage(state: string | null = null, action: FailureActions): string | null {
    switch (action.type) {
        case "RAISE_FAILURE": return action.message;
        case "CLEAR_FAILURE": return null;
    }
    return state;
}

const rootReducer = combineReducers<AppState>({
    accountInfoState,
    loadingMessages,
    failureMessage,
});

export default rootReducer;
