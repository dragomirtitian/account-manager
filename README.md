## prerequisites 

#### Server

The server is not included in the sources code, it will need to already be running when you start the app. By default the server is expected to run on `http://localhost:8080` but this can be changed in `src/config.json`.

### `npm install`

Run `npm install` before running the app

## Run - `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

##Notes

1. To see the loading UI there is config parameter `debugDelay` in `./src/config.json` that introduces and artificial delay on all requests 

2. The api requests are attempted 3 times before giving up. The error is displayed in the footer if a request has failed even as the request is attempted more times.

## Issues

There is a bit of an inconsistently in the way Credit/Debit is handled. In the sample data all amounts are positive. Some records have `to` (so I assume they are debits) others have `from` (so I assume they are credits). Given that all amounts are positive in the sample data I would have expected when adding a new credit/debit, the amount would be add or subtracted based on the existence of to/from, but this is not the case. 

My workaround: When adding a new debit, I send a negative amount to the server to ensure the balance is updated correctly. This leads to some debits with negative amounts (the newly added ones) and some with positive amounts (the original ones). To ensure consistent display, I actually take the `Math.abs` of amounts and add the `-` on all debits.